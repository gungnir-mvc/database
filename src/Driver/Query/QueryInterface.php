<?php
namespace Gungnir\Database\Driver\Query;
interface QueryInterface 
{
	public function getQuery() : String;
}