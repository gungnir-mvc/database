<?php
namespace Gungnir\Database\Driver\Query;

interface QueryPart
{
    public function getQueryPartString() : String;
}
