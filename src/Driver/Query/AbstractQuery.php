<?php
namespace Gungnir\Database\Driver\Query;

use Gungnir\Database\Table;

abstract class AbstractQuery implements QueryInterface
{
	private $driver = null;
	private $query  = null;
	private $table  = null;
	private $limit  = null;

	public function driver($driver)
	{
		$this->driver = $driver;
	}

	public function table(String $table = null, String $key = null)
	{
		if ($table) {
			$this->table = new Table($table, $key);
			return $this;
		}
		return $this->table;
	}

	public function execute(String $query)
	{
		if (strpos($query, 'LIMIT') !== true && is_null($this->limit) === false) {
			$query .= $this->limit;
		}
		return $this->driver->execute($query);
	}

	public function limit(Int $limit, Int $start = null)
	{
		$this->limit = new Limit($limit, $start);
		return $this;
	}

	abstract public function getQuery() : String;
}
