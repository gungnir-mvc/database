<?php
namespace Gungnir\Database\Driver;

use Gungnir\Database\Driver\Query\{Insert, Select, Update, Delete};

abstract class AbstractDriver implements DatabaseDriverInterface
{
	public function select(String $select, String $table = null)
	{
		$select = new Select($select);
		$select->driver($this);
		if ($table) {
			$select = $select->from($table) ?? $select;
		}
		return $select;
	}

	public function delete()
	{
		$delete = new Delete;
		$delete->driver($this);
		return $delete;
	}

	public function update(String $table)
	{
		$update = new Update;
		$update->driver($this);
		$update->table($table);
		return $update;
	}

	public function insert()
	{
		$insert = new Insert;
		$insert->driver($this);
		return $insert;
	}
}
