<?php
namespace Gungnir\Database\Driver;

use Gungnir\Core\Config as Config;

class Sqlite extends AbstractDriver
{
	private $config = null;
	private $connection = null;

	public function __construct(Config $config)
	{
		$this->connection = new \PDO('sqlite:' . ROOT . $config->database . '.db');
		$this->config = $config;
	}

	public function execute(String $query)
	{
		return $this->connection->exec($query);
	}

	public function query(String $query)
	{
		$sth = $this->connection->prepare($query);
		$sth->execute();
		return $sth;
	}

	public function config(Config $config = null)
	{
		if ($config) {
			$this->config = $config;
			return $this;
		}

		return $this->config;
	}
}
