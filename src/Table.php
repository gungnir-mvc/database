<?php
namespace Gungnir\Database;
class Table 
{
	private $privateKey = null;
	private $name = null;

	public function __construct(String $name, String $privateKey = null)
	{
		$privateKey = $privateKey ?? rtrim($name, 's') . '_id';
		$this->name($name);
		$this->key($privateKey);
	}

	public function __toString()
	{
		return $this->name();
	}

	public function key(String $privateKey = null)
	{
		if ($privateKey) {
			$this->privateKey = $privateKey;
			return $this;
		}
		return $this->privateKey;
	}

	public function name(String $name = null)
	{
		if ($name) {
			$this->name = $name;
			return $this;
		}

		return $this->name;
	}

}