<?php
namespace Gungnir\Database;

use Gungnir\Core\Config;
use Gungnir\Database\Driver\DatabaseDriverInterface;

class Database
{
	private $driver = null;
	private static $instances = [];

	public function __construct(DatabaseDriverInterface $driver)
	{
		$this->driver = $driver;
		self::$instances[$driver->config()->parent] = $this;
	}

	public static function factory(String $source)
	{
		$config       = new Config;
		$path         = 'config/database/Connections.php';
		$fallbackPath = VENROOT . '/gungnir-mvc/database/';

    $config->loadFromFileCascading($path, $fallbackPath . $path);

    $factory = new DriverFactory;
		$driver  = $factory->getDriverByConfig($config->$source);

    return new Database($driver);
	}

	public static function instance(String $source)
	{
		if (isset(static::$instances[$source])) {
			return static::$instances[$source];
		}

		return static::factory($source);
	}

	public function driver()
	{
		return $this->driver;
	}

	public function execute(String $query)
	{
		return $this->driver()->execute($query);
	}

	public function query(String $query)
	{
		return $this->driver()->query($query);
	}

	public function select(String $string, String $table = null)
	{
		return $this->driver()->select($string, $table);
	}

	public function insert()
	{
		return $this->driver()->insert();
	}

	public function delete()
	{
		return $this->driver()->delete();
	}

	public function update(String $table)
	{
		return $this->driver()->update($table);
	}
}
