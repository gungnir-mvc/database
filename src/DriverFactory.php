<?php
namespace Gungnir\Database;

use Gungnir\Core\Config;

class DriverFactory
{
	public function getDriverByConfig(Config $config)
	{
		$driver = null;
		switch ($config->get('driver')) {
			case 'mysql':
				$driver = $this->makeMysqlDriver($config);
				break;
			default:
				$driver = $this->makeSqliteDriver($config);
				break;
		}
		return $driver;
	}

	public function makeSqliteDriver(Config $config)
	{
		return new \Gungnir\Database\Driver\Sqlite($config);
	}

	public function makeMysqlDriver(Config $config)
	{
		return new \Gungnir\Database\Driver\Mysql($config);
	}
}
