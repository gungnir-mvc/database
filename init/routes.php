<?php
use Gungnir\HTTP\Route;

Route::add('database', new Route('/db/:controller/:action/:param', [
		'namespace' => '\Database\Controller\\',
		'defaults' => [
			'controller' => 'index',
			'action' => 'index',
			'param' => false
		],
	]
));